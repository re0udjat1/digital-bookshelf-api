package bookshelf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigitalBookshelfApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigitalBookshelfApiApplication.class, args);
	}

}
