package bookshelf.common.model;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class BaseModel {

    @Id
    private ObjectId id;

    private LocalDateTime timeCreated;

    private LocalDateTime timeUpdated;

}
