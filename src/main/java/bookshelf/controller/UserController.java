package bookshelf.controller;

import bookshelf.common.service.user.UserService;
import bookshelf.model.User;
import bookshelf.repository.UserRepository;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
@RequestMapping("/api/v1/users")
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @GetMapping(path = "/{userId}")
    public HttpEntity<Object> getById(@PathVariable(name = "userId") ObjectId userId) {
        User user = userService.getById(userId);

        // If user not found, assert failed message response
        if (user == null) {
            return ResponseEntity.badRequest().body("Failed");
        }

        // Assert success message response
        return ResponseEntity.ok("Success");
    }
}
