package bookshelf.model;

import bookshelf.common.model.BaseModel;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "users")
public class User extends BaseModel {

    private String username;

    private String password;

    private String firstName;

    private String lastName;

    private Date birthday;

}
